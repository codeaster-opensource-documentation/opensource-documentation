
###################################
 Summary
###################################

This is a 2D-test 2D in plane-stress quasi-static conditions and is a use-case in order to facilitate one's first steps with the salome_meca platform and develop a simplecase in linear elasticity. 

It consists in a homogeneous rectangular plate, with a hole in its center. The plate is loaded on its ends in tnesion. 

First (model A), a step-by-step approach in order to build the geometryn the mesh and the input data in AsterStudy is presented. Then (models B and C), one will carry out a mesh refinement.


.. note::

  This tutorial is presented as a reference for the other tutorial. It is a mere copy of forma01 within code_aster with a few corrections of the automated translation.


Credits
============


+------------------+-------------------+------------------------------------------------------+
| Contributor      | Organisation      | Logo                                                 |
+==================+===================+======================================================+
| Dominque Geoffroy| EDF               | .. image:: edflogo.png                               |
|                  |                   |    :width: 100px                                     |
|                  |                   |    :height: 60px                                     |
+------------------+-------------------+------------------------------------------------------+
