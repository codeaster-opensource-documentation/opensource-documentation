**********************************
Model B
**********************************

Objectives
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Geometry
^^^^^^^^^^

Mesh modifications
^^^^^^^^^^^^^^^^^^^

Quantities tested and results
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


+----------+------------------+-----------------------+-------------------------+-------------+
|   Mesh   | Localisation     | Parameter             | Reference value         |  Tolerance  |
+==========+==================+=======================+=========================+=============+
|   1      + Node B           | :math:`{\sigma}_{YY}` | 303.0                   |     5  %    |
+----------+------------------+-----------------------+-------------------------+-------------+
|   1      +  Node A          | :math:`{\sigma}_{XX}` | -100.0                  |     15 %    |
+----------+------------------+-----------------------+-------------------------+-------------+
|   2      + Node B           | :math:`{\sigma}_{YY}` | 303.0                   |     2  %    |
+----------+------------------+-----------------------+-------------------------+-------------+
|   2      +  Node A          | :math:`{\sigma}_{XX}` | -100.0                  |     3  %    |
+----------+------------------+-----------------------+-------------------------+-------------+
