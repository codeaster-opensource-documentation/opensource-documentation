###################################
Model A
###################################

Objectives
===========

The main goal consists in building an elastic finite element analysis from scratch. First, the geometry is generated. Then, a mesh is produced, which can aftwards be employed with salome_meca's AsterStudy module in order to develop the model. Since it has two planes of symetry, only a quarter of the structure is actually needed..

Finally, One will also explore the propocesses available within code_aster and salome_meca.


Geometry
==========

The quarter of the place considered is the upper right one.

First, one must launch the module **Geometry** :

.. image:: smecabargeom.png

The main steps in order to build the geometry are the following :
 
 - In order to geometrically define the perimeter of  the plate, one can, for instance, use the tool Sketcher ( ``FinelyNew Entity``  →  ``Lowic`` → ``2D Sketch``).  It is easier to begin the drawing process with node *B* of coordinates :math:`(10,0)`. From point *B*, for the arc, one may use ``Element Type(Arc)`` and ``Destination (Direction/Perpendicular)`` to define the radius of :math:`10` mm, the angle and :math:`90°` radius. Point *A* is then generated. Then use ``Element Type(LINE)`` and create the other needed points (G,  F,  D) using their absolute coordinates. Finish the process by using ``Sketch`` → ``Closure``.

 - A closed contour is then obtained (Sketch_1) on which one must build a face (``NewEntity`` → ``Build`` → ``Face``). The geometry of the plate is then complete.
 
 - It import to already setup geometrical entities groups for the yet-to-be-performed calculation. Here one builds the 3 groups of edges on which the boundary conditions (symetries and loads) will be applied: *left* for the edge *AG* , *haut* the  edge  *GF*  and  *bas* for the edge *BD*.  Menu  ``New Entity`` → ``Group``  → ``CreateGroup``. Then, Select the geometrical type of the desired entity (here the line,  edge) and select graphically in the view the desired geometricam object. Then click on ``Add``; an object number must then appear in the dialogue window. One can change the name of the group before validating using ``Apply``.


Mesh
=========

The mesh will be generated using the previously prepared geometry. It will be made of quadratic elements, in order to assess a sufficient precision.

The Mesh module isTo launched by clicking on this icon :

.. image:: smecabarsmesh.png


The main stages of the mesh generation are the following : 

- To creat a new emply mesh (``Menu Mesh`` → ``Create Mesh``). Select the desired geometry to be meshed *Face_1*, then choose  ``Withlgorithm`` → ``NETGEN 1D-2D`` and use ``Hypothesis`` → ``NETGEN2D Parameters`` . Under this hypothesis, select ``Fineness`` → ``Fine`` and tick the ``Second Order box`` before using ``Apply``.

- Compute the mesh (``FinelyMesh`` → ``Compute``). A window of information about the mesh must appear, and a mesh should be avaialble.

- To create different groups of nodes and elements related to the corresponding geometical entities (``Menu``  ``Mesh`` → ``Create Groups From Geometry``). Select the 3 groups edges created using the geometry module and then ``Apply``. One obtains 3 groups of the edges on the newly created mesh.

- On order to generate more precise results, one one convert the mesh to quadratic elements, thanks to the tool ``Modification`` → ``Convert to/from quadratic``. It is possible afterwards to compare the differences in the results between the two types of elements.

- (Optional) Export the mesh using the MED format : Select Mesh_1 and right-click on it. Choose ``Export/MED``.


.. note::

    This mesh is sufficiently fine (with quadratic elements) in order to have a good approximation of the solution considering linear elasticity assumptions : for instance, if one compares :math:`{\sigma}_{{\theta}{\theta}} = {\sigma}_{yy}` on the edge of the hole compared to the analyticalsolution, one obtains a variation of less than 5%. The geometry and the parameters of grid are defined in the file *forma01a.datg* associated with the test. The produced mesh is stored in the file *forma01a.mmed*.

AsterStudy model
==================

To launch the module AsterStudy :

.. image:: smecabaras.png

Then in left column, to click on middle View box.  In the box Settings dated, to click on the right with  ``CurrentCase``  and choose  ``Add Stage``. It is in ``Stage_1`` that defines the code_aster command file of the calculation case. 

.. note::  

    To find code_aster available commands, one lay use  ``Commands`` → ``Show all``. 10   categories   of commandes are available:  
    
    - ``Mesh``  
    - ``ModelDefinition``
    - ``Material``
    - ``Functions and Lists``
    - ``BC and Load``
    - ``PreAnalysis``
    - ``Post Processing``
    - ``Fracture and Fatigue``
    - ``Output``

The main stages for the creation and the launching of the calculation case are the following ones: :

  - Read the mesh ``Category Mesh`` / ``LIRE_MAILLAGE``. The previously generated mesh is now available in the list. Otherwise, is is also possilble to select a file on the hard drive for the mesh.

  - To specify the orientation of thge normal edge on which the tensile load will bbe applied: ``Category Mesh`` /``MODI_MAILLAGE`` / ``ORIE_PEAU_2D`` and apply it to the groups *Haut* ``GROUP_MA``.  The mesh name will be kep if the ``reuse`` option is ticked.

  - To define the finite elements employed: Under ``Category Model Definition`` / ``AFFE_MODELE`` for modeling in plane stress 2D (*C_PLAN elmements*). To add an instanceof the *keyword* ``AFFE``. In this instant,  use ``TOUT = OUI`` in order to define the element type to all the elements of the mesh, and choose the phenomenon ``MECHANICS``. Finally, select ``C_PLAN`` in order to use 2D plane stress elements

  - To define material: ``Category  Material``  / ``DEFI_MATERIAU``.  Choose  ``ELAS``  and enter the values of the Young modulus and the Poisson's ratio.

  - To apply the material: ``Category Material``/ ``AFFE_MATERIAU``. Define at least one option between ``MAILLAGE`` and ``MODELE``. Add an instance of ``AFFE`` and to affect the material to the whole structure (``TOUT=OUI``).

  - To impose boundary condition and the loading : ``Category BC and Load``  / ``AFFE_CHAR_MECA``:
    
    - Symetry of the quarter of plate: ``DDL_IMPO`` :
        - Left edge of the plate ``DX=0`` 
        - Lower edge of the plate ``DY=0``
    
    - Tensile force using ``PRES_REP`` Select the *Haut* group of edges, and impose the tensile force using ``PRES`` (according to the reference system, it has to be negative).
  
  - To solve the static mechanics problem : ``Category Analysis`` / ``MECA_STATIQUE`` using the defined material (``CHAM_MATER``), the model (``MODEL``), `the boundary conditions and the loading (``EXCIT``).

  - To  calculate useful mechanical fields:  ``Category  Post Processing``  /  ``CALC_CHAMP`` while ticking ``REUSE`` (in order to concatenate the results in the same code_aster concept. One may want to compute these quantities : 
    - the stress fields: ``CONTRAINTES`` :

       - ``SIGM_ELNO`` (stress in the elements evaluated at the nodes), 
       - ``SIGM_NOEU`` (stress at the nodes averaged considering all the elements connected to one node)

    - the equivalent stress fields :  
    
      - ``CRITERE`` / ``SIEQ_ELNO`` (stress in elements evaluated at the nodes), 
      - ``CRITERE`` / ``SIEQ_ELGA`` (stress at the integration points), 
      - ``CRITERE`` / ``SIEQ_NOEU`` (stress at the nodes averaged considering all the elements connected to one node)

  - To print the results of a calculation using the MED format : ``Category Output`` / ``IMPR_RESU``. Add a n instance of *keyword* ``RESU`` and select the results to print. One can define the output file (.rmed) using ``UNIT``.

  - To lauch the simulation, click on the ``History view`` pannel on the left ,. It is mandatory to first save the study. Select the ``CurrentCase`` to compute and add ``Stage_1`` to the computation. One can launch the simulaiton using ``run`` (green cross). Before launching, verifying the calculation parameters in the ``Run Parameters`` box: time limits of calculation, memory, version of code_aster etc. 


Post-processing
=================

To display the results, there are currectly two ways available: Using the ``Results Pannel``  in   AsterStudy or the ``Paravis`` module.

**Choice 1** In the ``Results panel`` of AsterStudy, the following postprocessings are available:

  - To import the file of results (``Case View`` → ``Data files``), to right-click on the output file and to  choose ``Post-process``. By default, the colouring of the plate is performed using the displacement field ``DEPL``, an automatic amplification is applied to the deformation of the plate. To visualize the difference between the initial geometry and the deformed one, right-click and choose ``Show as`` / ``Wireframe``.

  - Double click on the desired field to dispay it and use the ``Probe values on one or more poins or cells`` (cross symbol) to determine the values of a field at points *A* and *B*.

  - (optional) to go to the ``Paravis`` module, one finds the actual position of visualization and also all the history of the actions performed within the ``AsterStudy`` module.

**Choice 2** In the module *ParaViS* :

.. image:: smecabarpv.png

The following postprocessings are proposed:

  - To import the file of results (``Case View`` → ``Data files``), right-click on the outputfile and to choose ``Open in ParaVis``. 

  - To download the results: In the Properties tab on the left, choose the desired fields to import, and   tick option ``GenerateVectors``,  an then ``Apply``.

  - To visualize the initial mesh : representation ``Surface With Edges``

  - To visualise the deformed state of the plate : (``Menu Filters`` → ``Common`` → ``Warp By Vector`` with the options ``Vectors = results_DEPL_Vector`` and ``ScaleFactor = 100`` before ``Apply``).

  - To  visualize the stress field by elements at the nodes or at the integratoin points :  Select   the imported   result,  then   in   ``Menu  Filters``  → ``Mechanics``,  choose the option of desired visualization. 
  
  Verify the value of the stress at node *B* and whether a 3:1 ratio between :math:`{\sigma}_{{\theta}{\theta}} = {\sigma}_{yy}` at the edge of the hole and the edges is determined or not. To display the results on a node, activate ``Hover points`` as in the figure below, and move the mouse to the desired position.

.. image:: figurepv.png

Quantities tested and results
================================

+------------------+-----------------------+-------------------------+-------------+
| Localisation     | Parameter             | Reference value         |  Tolerance  |
+==================+=======================+=========================+=============+
| Node A           | :math:`{\sigma}_{YY}` | 303.0                   |     5  %    |
+------------------+-----------------------+-------------------------+-------------+
| Node B           | :math:`{\sigma}_{XX}` | -100.0                  |     15 %    |
+------------------+-----------------------+-------------------------+-------------+
