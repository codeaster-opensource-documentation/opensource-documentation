###################################
Reference Problem
###################################

Geometry
===========

The structure studied is a rectangular plate, with a hole in its middle. It is modelled using 2D plane stress finite elements. One only has to model a quarter of the plate thanks to the structure's two symetry planes. Dimensions are given in millimetres.

.. image:: figure1.png
   :width: 550px

Boundary condtions and loadings
=================================

**Symetries** :The plate is blocked in direction *Ox* along the *AG* edge and in directon *Oy* along the *BD* edge.

**Loading** :A tensile loading of :math:`P = 100` MPa is distributed along the *FG* edge of the plate.


Material properties
======================

The characteristics employed are the following: 

- Young modulus :math:`E = 200 000` MPa
- Poisson's ratio :math:`{\nu} = 0.3`
- Yiels stress : :math:`{\sigma}_Y = 200` MPa


Elastic solution
========================

Considering :

- An elastic material, 
- An infinite plane,
- A hole of diameter :math:`a`
- A load of :math:`P`, 

The analytical solution under plane stress conditions using polar coordinates :math:`(r, {\theta})` is [bib1]:

.. math::

   {\sigma}_{rr}= \frac{P}{2} \Big[ \Big( 1 - \big( \frac{a}{r} \big) ^2 \Big) - \Big( 1 - 4 \big( \frac{a}{r} \big) ^2 + \big( \frac{a}{r} \big) ^4 \Big) \cos(2{\theta})\Big]


   {\sigma}_{{\theta}{\theta}}= \frac{P}{2} \Big[ \Big( 1 + \big( \frac{a}{r} \big) ^2 \Big) + \Big( 1 + 3 \big( \frac{a}{r} \big) ^4 \Big) \cos(2{\theta})\Big]
   
      {\sigma}_{{r}{\theta}}= \frac{P}{2} \Big[ \Big( 1 + 2 \big( \frac{a}{r} \big) ^2 \Big) - \Big( 1 3 \big( \frac{a}{r} \big) ^4 \Big) \sin(2{\theta})\Big]

Specifically at the edge of the hole (  :math:`r=a` ) : 

.. math::

  {\sigma}_{{\theta}{\theta}} = P \Big [ \big (1 + 2 \cos (2 {\theta} ) \big) \Big]

And along the :math:`x` axis :

.. math::
   {\sigma}_{{\theta}{\theta}}= {\sigma}_{yy} = \frac{P}{2} \Big[ \Big( 1 + \big( \frac{a}{r} \big) ^2 \Big) + \Big( 1 + 3 \big( \frac{a}{r} \big) ^4 \Big) \Big]
  
Hence, for :math:`P=1` MPa, and for an infinite plate, one gets :

+---------+-----------------------+------------------------------------------------------------+------------+
| Node    | Parameter             | Reference value                                            | Tolerance  |
+=========+=======================+============================================================+============+
| Node A  | :math:`{\sigma}_{XX}` | :math:`{\sigma}_{{\theta}{\theta}}(r=a,{\theta}= {\pi}/2)` |    -1      |
+---------+-----------------------+------------------------------------------------------------+------------+
| Node B  | :math:`{\sigma}_{YY}` | :math:`{\sigma}_{{\theta}{\theta}}(r=a,{\theta}= 0`        |    3       |
+---------+-----------------------+------------------------------------------------------------+------------+

For a plate of finite dimension,, the abacuses in [bib1] can be employed in order to obtain stress concentration factors, and one finds that for a tensile force of 1 MPa, the maximum of :math:`{\sigma}_{YY}` is approximately 3.03 MPa at point *B*.
  
Litterature references
=======================

- **VOLDOIRE F.**, *Analyses limites de structures fissurées et critères de résistance*, Note EDF/DER/HI/74/95/26 1995.
- **PETERSON R.E.**, *Stress concentration factors*, Ed. J. WILEY p.150.
