##############
Studies
##############

These tests-cases are additional tests for code_aster developed by the open source community. They can be used as reference material in order to assess models implementation. The comparisons are performed using analytical solutions or simplified simulations.
    

*****************
Thermomechanics
*****************

.. toctree::
    civilengineering/index
    :maxdepth: 1


**********************************
Additive Manufacturing
**********************************

**********************************
Welding
**********************************

*****************
ROM
*****************
