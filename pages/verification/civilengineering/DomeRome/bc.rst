##########################################
Boundary conditions and loadings
##########################################

It is assumed that the base of the dome is supported only against vertical loading.

The displacement in the other orthogonal directions is restrained on one node in order to prevent rigid body motion. 

Only the self-weight is considered.
  

