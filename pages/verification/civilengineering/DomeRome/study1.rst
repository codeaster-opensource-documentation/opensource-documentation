##########################################
FEA using Coque_3D 
##########################################

.. code:: python

         # -*- coding: utf-8 -*-
         import numpy as np
         
         ############################################ 
         
             # DOME ANALYSIS : PANTHEON OF ROME #  
         
         ############################################
         
         #### READING MESH IN ASTER FORMAT #### 
         DEBUT(PAR_LOT ='NON',);
         
         MAIL=LIRE_MAILLAGE(
             INFO=1,
             UNITE=20,
             FORMAT='MED',
             VERI_MAIL = _F(VERIF= 'NON'));
         
         ### CREATING MESH GROUPS  #### 
         MAIL = DEFI_GROUP(reuse = MAIL,
         MAILLAGE = MAIL, 
         CREA_GROUP_MA = (_F(NOM = 'DOME',TOUT = 'OUI'),
                          _F(NOM = 'QUAD', TOUT = 'OUI', TYPE_MAILLE  = 'QUAD4',)),
         CREA_GROUP_NO = (_F(TOUT_GROUP_MA =  'OUI')))

         ### CHECKING IF THE NORMAL VECTORS ARE ORIENTED CORRECTLY ####
         MAIL= MODI_MAILLAGE(reuse = MAIL,
                             MAILLAGE = MAIL,
                             ORIE_NORM_COQUE = _F(GROUP_MA= 'QUAD')),    
                    
         ### CREATING A QUAD9 MESH ####
             ### PASSING FROM LINEAR TO QUAD8 ELEMENTS ###
         MeshQuad=CREA_MAILLAGE(MAILLAGE=MAIL,
                                LINE_QUAD=_F(GROUP_MA=('DOME',),
                                             #TOUT='OUI',
                                             PREF_NOEUD='NQ',),);
                                    
         ### PASSING FROM QUAD8 TO QUAD9 ###                      
         Mesh=CREA_MAILLAGE(MAILLAGE=MeshQuad,
                            MODI_MAILLE=_F(GROUP_MA='DOME',
                            OPTION='QUAD8_9',
                            PREF_NOEUD='NT'
                            ),);
                                    
         ### CREATING A FE MODEL FOR A MECHANICAL PROBLEM USING COQUE_3D  ###
         MO=AFFE_MODELE(MAILLAGE=Mesh,
                        INFO=1,
                        AFFE=(_F(TOUT='OUI',PHENOMENE='MECANIQUE',MODELISATION='COQUE_3D',))),

         ### CREATING / ASSIGN A MATERIAL ####
         BETON=DEFI_MATERIAU(ELAS=_F(E=2.9E9,
                                     NU=0.2,
                                     RHO=1350),)


         MATER=AFFE_MATERIAU(MAILLAGE=Mesh,
                             AFFE=(_F(TOUT='OUI',
                                      MATER=BETON,),),)
 
        ### DEFINING THE DOME'S THICKNESS ###
        CAEL=AFFE_CARA_ELEM(MODELE=MO,
                	     COQUE = (_F(EPAIS = 1.5,
                                	 GROUP_MA= 'DOME',)),),

        ### BOUNDARY CONDITIONS AND LOADINGS ###
        COND_0=AFFE_CHAR_MECA(MODELE=MO,
                              DDL_IMPO=(_F(GROUP_MA=('BOTTOM',),
                                  	    DZ =0,), ## VERTICAL DISPLACEMENT AT THE BOTTOM NODES
                                	 _F(GROUP_NO=('N_RIGHT',),
                                	    DX =0),
                                	 _F(GROUP_NO=('N_UPPER',),
                                	    DY =0)),
                	      PESANTEUR=_F(DIRECTION =(0,0,-1), GRAVITE = 9.8,)), ## SELF WEIGHT

        ### SOLVING THE PROBLEM ###
        RESU = MECA_STATIQUE(MODELE=MO,
                	     CHAM_MATER=MATER,
                	     CARA_ELEM=CAEL,
                	     EXCIT=_F(CHARGE=COND_0,
                        	     TYPE_CHARGE='FIXE_CSTE',),
                	     OPTION='SIEF_ELGA',
                	     SOLVEUR=_F(RENUM='AUTO',
                                	 NPREC=8,
                                	 ELIM_LAGR='LAGR2',
                                	 STOP_SINGULIER='OUI',
                                	 TYPE_RESOL='AUTO',
                                	 ACCELERATION='AUTO',
                                	 LOW_RANK_SEUIL=0.0,
                                	 PRETRAITEMENTS='AUTO',
                                	 POSTTRAITEMENTS='AUTO',
                                	 PCENT_PIVOT=20,
                                	 RESI_RELA=4.E-06,
                                	 GESTION_MEMOIRE='AUTO',
                                	 MATR_DISTRIBUEE='NON',
                                	 METHODE='MUMPS',),
                	     INFO=1,)

        ### POST PROCESSING THE RESULTS ###
        RESU=CALC_CHAMP(reuse =RESU,
                	 MODELE = MO,
                	 RESULTAT =RESU,
                	 CONTRAINTE = ('SIGM_ELNO','SIGM_ELGA'),
                	 FORCE = ('REAC_NODA'),),

        ### CALCULATING THE VERTICAL REACTION AT THE BOTTOM ###                
        REAC =POST_RELEVE_T(
                	     ACTION=(_F(
                        	     INTITULE= 'BOTTOM REACTIONS',
                        	     GROUP_NO= 'BOTTOM',
                        	     RESULTAT= RESU,
                        	     NOM_CHAM='REAC_NODA',
                        	     TOUT_ORDRE='OUI',
                        	     RESULTANTE= ('DZ' ) ,
                        	     OPERATION='EXTRACTION',)));

        ### POST PROCESSING THE STRESS RESULTS ###                 
        stat2=POST_CHAMP(
                	     RESULTAT=RESU ,
                	     GROUP_MA= ('DOME' , ) ,
                	     EXTR_COQUE=_F(
                	     NUME_COUCHE=1 ,
                	     NIVE_COUCHE='SUP' ,
                	     NOM_CHAM= ('SIGM_ELNO','SIGM_ELGA',)),);


        statsup=CALC_CHAMP(
                	 RESULTAT=stat2 ,
                	 GROUP_MA= ('DOME',) ,
                	 CRITERES= ('SIEQ_NOEU','SIEQ_ELNO','SIEQ_ELGA'),) ;

        ### PRINTING PRINCIPAL STRESSES IN MED FORMAT ###              
        IMPR_RESU(UNITE = 11, 
        	   FORMAT = 'MED',
        	   RESU=_F(GROUP_MA = 'DOME',
        	   RESULTAT= statsup,
        	   NOM_CHAM = ('SIEQ_ELNO','SIEQ_ELGA'))),


        ### PRINTING GENERAL STRESSES IN RESU FORMAT ###
        IMPR_RESU(UNITE = 13, 
        	   FORMAT = 'RESULTAT',
        	   RESU=_F(MAILLAGE= MAIL,
                	   RESULTAT= stat2,
                	   NOM_CHAM = 'SIGM_ELGA', 
                	   GROUP_NO ='DOME'),),


        ### PRINTING PRINCIPAL STRESSES IN RESU FORMAT ###
        IMPR_RESU(UNITE = 14, 
        	   FORMAT = 'RESULTAT',
        	   RESU=_F(MAILLAGE= MAIL,
                	 RESULTAT= statsup,
                	 NOM_CHAM = 'SIEQ_ELGA', 
                	 GROUP_NO ='DOME', 
                	 NOM_CMP =('PRIN_1','PRIN_2', 'PRIN_3')),),


        ### PRINTING VERTICAL REACTION AT THE BOTTOM ###
        IMPR_TABLE (TABLE=REAC, UNITE = 15, FORMAT = 'TABLEAU',),


        FIN();
  

