##########################################
Conclusion
##########################################

In this application it is possible to visualize the common structural behavior of domes. Hoop forces (tension) are higher at the base and meridional (compression) forces also increase from the crown to the base. 

The results agree reasonably well with the ones proposed by Morer and Goñi [1].  

Vertical reaction at the base matches with the analytical value. 
  

