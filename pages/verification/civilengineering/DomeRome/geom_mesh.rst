##########################################
Geometry, Mesh and material properties
##########################################

The Pantheon's dome consists of a hemisphere with an interior radius of 21.81 m and a thickness of 1.5 m. At the top, there is an opening with a radius equal to 4.5 m. 

The mesh contains QUAD9 elements. 

.. figure:: geometry.jpg
   :name: dome-geometry
   
   The model's dome
   
The isotropic elastic material is characterized by the following properties::

  - Young Modulus = 2.9E9 Pa
  - Poisson's ratio = 0.2
  - Unit weight = 1350 kg/m3 
  

