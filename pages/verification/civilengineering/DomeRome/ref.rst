##########################################
Reference Solution
##########################################

Morer and Goñi [1] provide the internal hoop and meridional stress at the base of the dome, such as : 

Max. Hoop  = 2.2E5 Pa 

Min. Meridional = -2.9E5 Pa 

The vertical reaction at the bottom can be calculated considering the dome's volume, which leads to 4πR²tρg  = 6.3E7 N (not considering the opening at the top)

[1] Morer, P. e Goñi, R. 2008. A benchmarking study of the analysis of non-reinforced structures applied to the structural behavior of domes. [autor do livro] Dina D'Ayala e Enrico Fodde. Structural Analysis of Historic Construction. Londres : Taylor & Francis Group, 2008, pp. 705-713.


  

