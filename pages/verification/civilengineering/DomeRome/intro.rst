###################################
 Summary
###################################

This document will explain the step by step way to modelize and dome of Rome's Pantheon using salome_meca

From a global point of view, this application presents one of many ways of how curved structures can be analyzed with Code_Aster. More precisely, this model makes a principal stress analysis of the Pantheon's dome. The results are compared with Morer and Goñi [1].  

The different steps are the following :

- Creating the geometry and the mesh
- Boundary conditions and loading
- Reference Solution
- FEA approach : Coque_3D approach
- FEA approach : Solid shells approach
- Conclusions
- References



Credits
==========



+------------------+-------------------+------------------------------------------------------+
| Contributor      | Organisation      | Logo                                                 |
+==================+===================+======================================================+
| Igor Barcelos    | DeepMech          | .. image:: deepmechlogo.png                          |
|                  |                   |    :width: 150px                                     |
|                  |                   |    :height: 70px                                     |
+------------------+-------------------+------------------------------------------------------+



