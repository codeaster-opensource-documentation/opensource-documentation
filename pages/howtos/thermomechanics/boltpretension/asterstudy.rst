######################################################################
Data setting for the simulation in AsterStudy
######################################################################
In this part, all the parameters of the simulation will be implement.

Read the mesh
================

The compound mesh previously created needs to be selected.

Definition of the model
=========================

The type of modelisation will be selected, here 3D modelisation is chosen because there are only 3D elements. The phenomenon of the study will be mechanic and those choices are applied on all the assembly. The chosen mesh is the one create in the mesh reading section.

Material
============

First the definition of the material is needed depending on the study. Then the previously created material is assign to the model. In this example this material is assigned to all the study so the mesh and model previously defined are selected.
In this section it is possible to assign a different material for the nut and the bolt just by creating both material and assigning them on each parts.

Functions and Lists
======================

This section will define the times steps of the simulation. In this example, and as it is describe in the Step 2, there are two steps of simulation. Therefore, the creation of these two steps is needed with the command DEFI_LIST_REEL from 0 to 2 (the number of the time steps can be adjust depending on the study).
After the creation of the DEFI_LIST_REEL you will need to assign it to an instant list with the command DEFI_LIST_INST by selecting, in the time step list definition, the previous list of reals that has been created.

BCs and Loads
==============

This section will define the boundary conditions and loads that will be used in the simulation. In this example there are four mechanical loads needed (description in Step 2). This is where the definitions of the groups of Step 2 takes all its importance because all the loads will be assigned to the correct groups created in the Geometry part.

+------------+-------------------+--------------------------------------------------------------------------+
| N          | BC and LOAD       | Where                                                                    |
+============+===================+==========================================================================+
| 1          | Liaison ENCASTREE | Fixing the bolt in :math:`x=0`                                           |
+------------+-------------------+--------------------------------------------------------------------------+
| 2          | DDL_IMPO          | Blocking DX, DY, DZ in the lower part of the nut                         |
+------------+-------------------+--------------------------------------------------------------------------+
| 3          | FORCE_FACE        | In :math:`x=L` on the bolt (doing the pretension)                        |
+------------+-------------------+--------------------------------------------------------------------------+
| 4          | LIAISON MAIL      | Between the inner face of the nut of the outter face of the bolt         |
+------------+-------------------+--------------------------------------------------------------------------+

Analysis
============

This section will set up the simulation. Here the different BCs & loads will be assign to the corresponding time step. In this example we will create two STAT_NON_LINE analysis 

In the first STAT_NON_LINE we will activate BC N°1, N°2 and N°3 from :math:`t=0` to :math:`t=1`

.. code:: python

        resnonl0 = STAT_NON_LINE(CHAM_MATER=fieldmat,
                                COMPORTEMENT=_F(DEFORMATION='PETIT',
                                                RELATION='ELAS',
                                                TOUT='OUI'),
                                CONVERGENCE=_F(ITER_GLOB_MAXI=100,
                                                RESI_GLOB_RELA=1e-05),
                                EXCIT=(_F(CHARGE=Fixe1,
                                        TYPE_CHARGE='FIXE_CSTE'),
                                        _F(CHARGE=Traction,
                                        TYPE_CHARGE='FIXE_CSTE'),
                                        _F(CHARGE=Fixe2)),
                                INCREMENT=_F(INST_FIN=1.0,
                                            LIST_INST=times),
                                METHODE='NEWTON',
                                MODELE=model0,
                                NEWTON=_F(MATRICE='TANGENTE',
                                        PREDICTION='ELASTIQUE',
                                        REAC_INCR=1,
                                        REAC_ITER=1),
                                SOLVEUR=_F(METHODE='MUMPS',
                                            RENUM='AUTO'))



In the second STAT_NON_LINE we will activate BC N°1, N°2 and N°4 from from :math:`t=1` to :math:`t=2`. The LIAISON_MAIL will be “DIDI” load type so it will impose a boundary condition relatives to the initial position.

.. code:: python

        resnonl0 = STAT_NON_LINE(reuse=resnonl0,
                                CHAM_MATER=fieldmat,
                                COMPORTEMENT=_F(DEFORMATION='PETIT',
                                                RELATION='ELAS',
                                                TOUT='OUI'),
                                CONVERGENCE=_F(ITER_GLOB_MAXI=100,
                                                RESI_GLOB_RELA=1e-05),
                                ETAT_INIT=_F(EVOL_NOLI=resnonl0,
                                            INST_ETAT_INIT=1.0),
                                EXCIT=(_F(CHARGE=Fixe1),
                                       _F(CHARGE=Fixe2),
                                       _F(CHARGE=Tie,
                                          TYPE_CHARGE='DIDI')),
                                INCREMENT=_F(INST_FIN=2.0,
                                            LIST_INST=times),
                                METHODE='NEWTON',
                                MODELE=model0,
                                NEWTON=_F(MATRICE='TANGENTE',
                                        PREDICTION='ELASTIQUE',
                                        REAC_INCR=1,
                                        REAC_ITER=1),
                                SOLVEUR=_F(METHODE='MUMPS',
                                            RENUM='AUTO'))


.. note:: When giving the name of the second analysis, do not forget to check the box “reuse the input object” so it will take the end of the Analysis 1 as the Initial condition of Analysis 2.

.. note:: 'DIDI' stands for DIRICHLET DIFFERENTIEL. It allows to impose a boundary condition relatively to another one. For the current case, BC 4 is thus imposed relatively the last computed state. As a conquence, the LIAISON_MAILLE condition is imposed in the deformed configuration and note the initial one.

Post Processing
===============
The setup of the post processing data will depend on the case and what you what results are wanted. The result that will be selected is the analysis previously created so the simulation will give the result on the STAT_NON_LINE analysis.

Output
============
In this part the output files and format is chosen.



