###################################
Create the mesh
###################################

In this document a 3D part was created so a 3D mesh is used. Two mesh will be neededed: one for the bolt and one for the nut as shown below

.. figure:: meshcreation.png
   :name: fig4
   
   Meshing the bolt

.. figure:: meshcreation2.png
   :name: fig5
   
   Meshing the nut

After the creation of the meshes, the groups previously created in the geometry parts need to be created in the new meshes. In order to do, the “Create Groups from Geometry” option can be employed

Then, to create a compound of both meshes:

.. figure:: meshcreation3.png
   :name: fig6
   
   Creating a compound of both meshes

.. note:: 

  It is important to unselect the box surrounded in red so the coincident nodes between the bolt and the nuts will not be merged. Otherwise, two independant nodes (such as one part of the nut and the other one part of the bolt) can be fused because of they can be very close to each other.



