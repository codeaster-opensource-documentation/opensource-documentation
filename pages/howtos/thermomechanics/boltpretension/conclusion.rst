*****************************
Running the simulation 
*****************************

Once all the analysis data are implemented, the simulation can be run by selecting the current case. At the end of the simulation the results can be open by right clicking in the output file and selecting “Open with ParaVis”
