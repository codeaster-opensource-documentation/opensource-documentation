###################################
Creating the geometry
###################################

In this part the choice was to modelize the bolt and the nut as 3D models (but can be modelize as a 1D model).
To modelize it everything is done in the Shaper and Geometry module. 

.. figure:: bolt1.png
   :name: fig1
  
   Model that will be built-up

Create the groups and partition
=================================

Create the groups and partition which will be used in the simulation  In this part the biggest work is to choose the good groups and create the useful partitions regarding the steps of simulation, the boundary conditions and the loads that will be used in the simulation. 
In this pretension example the steps of the simulation are:

  * From :math:`t= 0` to :math:`t= 1` the bolt receive the pretension (F) from one side, is fixed in :math:`x=0` and there is no contact between the bolt and the nuts. Therefore, the lower surface of the nuts will be fixed in the space 
  * From :math:`t= 1` to :math:`t= 2` the tie connection between the nut and the bolt will be activate and the pretension force will be release. 

Regarding the previous steps the boundary conditions and loads in the simulation will be:

  - The bolt is fixed in :math:`x=0`
  - The bolt receive a pretension (A force in Newton depending on the chosen units system) in :math:`x=L`
  - The nut is fixed in the space from :math:`t= 0` to :math:`t= 1` while there is no contact with the bolt
  - There is a Tie connection between the outer surface of the bolt and the inner surface of the nuts starting at :math:`t = 1`
  - The groups that needs to be created from this analysis are the following:


The Nut
============

.. figure:: schema1.png
   :name: fig2
   
   The nut

The Bolt
============
First a partition should be create to make the contact surface of the nut on the bolt (this is useful to refine the mesh on the contact zone to see what happen). When the partition is created the following groups can be created: 

(The volume shape group will be useful to define the master node during the creation of the Tie Connection)

.. figure:: schema2.png
   :name: fig3
   
   The bolt
