###################################
 Summary
###################################

This document will explain the step by step way to modelize and simulate a pretension in a bolt.

The different steps are the following :

- Creating the CAD parts of the bolt and the nuts
- Creating the CAD partitions
- Ceating the Mesh
- Data input in AsterStudy
- Running the simulation



Credits
==========



+------------------+-------------------+------------------------------------------------------+
| Contributor      | Organisation      | Logo                                                 |
+==================+===================+======================================================+
| Louis Bachelot   | Bureau Veritas    | .. image:: bvlogo.jpg                                |
|                  |                   |    :width: 150px                                     |
|                  |                   |    :height: 70px                                     |
+------------------+-------------------+------------------------------------------------------+
| Dominque Geoffroy| EDF               | .. image:: edflogo.png                               |
|                  |                   |    :width: 100px                                     |
|                  |                   |    :height: 60px                                     |
+------------------+-------------------+------------------------------------------------------+


