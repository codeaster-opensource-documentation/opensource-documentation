**********************************
 Summary
**********************************
The following user guide is aimed at users of salome_meca 2019 wishing to exploit the OpenTURNS python library for uncertainty quantification (UQ). It is assumed that the user has familiarity with building and running models in AsterStudy but no prior knowledge of UQ or OpenTURNS. This guide will focus on providing users with familiarity with the practical setup of UQ studies via the OpenTURNS module that is integrated within salome_meca.



Credits
========


+------------------+-------------------+------------------------------------------------------+
| Contributor      | Organisation      | Logo                                                 |
+==================+===================+======================================================+
| Jefri Draup      | EDF               | .. image:: edfenergylogo.png                         |
|                  |                   |    :width: 150px                                     |
|                  |                   |    :height: 60px                                     |
+------------------+-------------------+------------------------------------------------------+


References
===========

[1] A. Pasanisi and A. Dutfoy. An industrial viewpoint on uncertainty quantification in simulation: stakes, methods, tools, examples. In 10th Working conference on uncertainty quantification in scientific computing (WoCoUQ), 2011.

[2] E. Ardillon, A. Dumas, and P. Bryla. Probabilistic optimisation of margins for plastic collapse in the mechanical integrity diagnoses of penstocks. In Congrès Lambda Mu 21 "Maîtrise des risques et transformation numérique: opportunités et menaces", 2018.

[3] P.T. Roy, N. El Mo, S. Ricci, J.C. Jouhaud, N. Goutal, M. De Lozzo, and M.C. Rochoux. Comparison of polynomial chaos and Gaussian process surrogates for uncertainty quantification and correlation estimation of spatially distributed open-channel steady flows. Stochastic Environmental Research and Risk Assessment, 32:1723–1741, 2018.
