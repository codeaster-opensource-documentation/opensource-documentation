########################################################################
General Workflow of UQ in salome_meca
########################################################################

Before we get into the details of how to build UQ studies, it is worth understanding the general workflow within salome_meca and where the OpenTURNS fits. :numref:`fig3` shows the typical workflow in the salome_meca environment. Typically, the user starts in the pre-processing modules (GEOM and MESH) and works systematically through the normal steps in developing FE models. The user will then develop and run their simulations, interactively, within AsterStudy.

.. figure:: fig3.png
   :name: fig3ot

   General workflow in salome_meca

At this point, the user has the option to visualise and post-process results using ParaVis, in a classical manner. However, if the user desires to perform UQ studies on their models, they must port their model into OpenTURNS. Within the OpenTURNS module, the user can develop their parametric studies and utilise other features of this powerful toolkit. Hence, the user remains within a single user-friendly environment to perform detailed studies on their numerical models.

Inside the OpenTURNS module
===============================

:numref:`fig4ot` shows the main widget within the OpenTURNS module; it is dynamic and interactive, helping the user develop studies intuitively. The widget has automatic gateways built-in, which forces the user to specify all pre-requisites to each stage of the UQ process, thereby eliminating basic mistakes. As OpenTURNS handles more than just UQ studies, the widget has several tabs that are dedicated to other features of the library, e.g. optimisation.

.. figure:: fig4.png
   :name: fig4ot

   General workflow of OpenTURNS in salome_meca
   
For all cases, the user starts with the Model definition tab, as per Stage 1 defined in §1.1. This is where the user chooses to build a study from analytical expressions (symbolic models), more complex python scripts (python models) or from graphical FE models (YACS model). It is always simplest to start building an UQ study from symbolic models as these are fastest. Once your model is defined, the gateways to sub-choices are unlocked. The main tabs available to the user are explained below:

 1. Model evaluation: This acts as a simple calculator for the user to get an insight into their model quickly. The user works more cleanly within the GUI instead of having Salome-Meca and another tool open at the same time. It should be used to check that you description of any model is functioning as expected.

 2. Screening: Effectively, this is a coarse filter of sensitivity that runs quickly. If the user has a problem with a large number of independent variables, this is a tool for reducing the number of variables to a sensible amount for further detailed study. The Morris method is a common method in applied statistics for global sensitivity analysis.

 3. Optimisation: This is a detailed analysis tool enabling the user to determine maxima/minima of their models. A choice of methods are available, such as truncated Newton method.

 4. Design of Experiments (DOE): This is a tool for creating sampling sets for the user model. This is a key aspect in propagating uncertainties, as per Stage 3 defined in §1.1.

 5. Probabilistic model definition (PMD): This is a detailed analysis tool enabling the user to understand the sensitivity of models outputs to the variance in user inputs; as per Stage 2 defined in §1.1. Quantified sensitivity indices can be extracted together with the trends in model outputs and limits. As can be seen in :numref:`fig4ot`, the sensitivity sub-tab, which is how UQ ranking assessments are performed (Stage 4 defined in §1.1) can only be selected if all prior stages are completed.
 
.. note::

  Tip: Setting up a UQ study with the widget

  In order to perform a UQ study, the user should interact with the widget as follows:
  
    1. Specify your model in the Model definition tab (UQ Stage 1)

    2. Set the input uncertainties under the PMD tab (UQ Stage 2)
  
    3. Create an input sample set from the DOE creation tab (UQ Stage 3) 

    4. Propagating these uncertainties (UQ Stage 3) and ranking the uncertainties (UQ Stage 4) under the PMD (Sensitivity) sub-tab

  
2.2	Before getting started
===================================

Before diving into a UQ study, it is worth knowing some useful tips or points that will help you to be more efficient.

Setup your UQ study separately
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

No matter what kind of model you intend to work with, it is helpful to separate your thought processes in terms of the model to be assessed, :math:`Y = G(X)`, and the UQ study itself. It is likely that a user wants to port their AsterStudy FE model into OpenTURNS, and details of that are shown in §3.1. However, it is always useful to develop your UQ study separately and use a basic symbolic model in order to speed this up. If you try and tune your UQ study using a complex FE model, you may find it frustrating because you have to wait for all your simulations to run. §3.2 will walk you through the UQ aspects in isolation, and point out some handy tips to help you develop a robust UQ study faster. 

Working with FE models from AsterStudy
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

when :math:`G(X)` is an FE model, you should be aware that only code_aster concepts are linked with OpenTURNS. For example, if you wanted to assess pipe thickness as an input variable, you cannot set a GEOM or SHAPER variable and link it to the mesh as these modules are not connected with OpenTURNS. However, if you were to model a pipe using shell elements, then shell thickness is a valid input variable as it is an object within code_aster. Hence, you should consider what is possible to be assessed within your FE model before you start.

Ensure your input/output variables are valid
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When working with FE models from AsterStudy, another point to consider is that you can only have single value inputs and single value outputs. For inputs this is often intuitive as you may be dealing with parameters like Young’s modulus or yield stress. For the outputs, you have to consider that FE outputs are often field values. For compatibility with OpenTURNS, the outputs must be single value outputs in the form of Numpy Tables. This may mean that your code_aster command files need to be adapted such that you filter the relevant outputs; an example of this is given in §3.1.

Input marginals
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Normal distributions are suitable probability density functions that cover a wider variety of natural properties. Unless you have supporting data or specific knowledge, a good starting point for most problems is the normal distribution. In terms of parameters that define mechanical properties, a distribution with mean, :math:`\mu`, and standard deviation, :math:`\sigma ±0.05 \mu`, that is known with a 95% confidence limit can be considered as an input marginal that is known with high certainty. This may not be true in different fields such as physics, but for mechanical engineering studies this could be considered as a rule of thumb. It would be wise to start with a case of high certainty and begin to relax your certainty, depending on prior knowledge. This will allow you to understand whether your study makes sense in comparison to some calculation or known solution before you try and interpret the effects of reduced certainty. In addition, input marginals and sample sizes are often inter-related; it makes sense to consider definition of the input marginal with the sample size in mind.

Sample size
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When considering a design of experiments, the concept of statistical significance should drive your study. In natural sciences, a sample size of the order 103−104 is considered necessary before the results from a sample can be considered significant. The potential computational cost is why it is sensible to develop your UQ study using a fast symbolic model first.


.. note:: 

    Determining sample size : Using the concept of the confidence interval we can relate the desired input marginal properties to the sample size: 

.. math::

    \overline{x} ± z  \frac{s}{\sqrt{N}}

where :math:`\overline{x}` is the sample mean, :math:`z` value represents the confidence interval (:math:`z = 1.96` for a 95% confidence interval), :math:`s` is the sample standard deviation, and :math:`N` is the sample size. If you are trying to achieve an input marginal with a specific confidence interval, this will help you determine the sample size necessary to do that.
    
Sampling strategies
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sampling with statistically significant numbers can easily result in computationally demanding assessments. Hence, before committing to a detailed study on an FE model, it makes sense to optimise your sampling strategy with the symbolic model as it is quick to run. There are a range of approaches to sampling, and if you are running models that are not symbolic, it makes sense that you utilise the Latin Hpercube sampling strategy. Unlike the Monte-Carlo (random sampling), Latin Hypercube Sampling (LHS) is an efficient sampling methodology for problems with large quantities of input variables. The LHS method should really be the default approach if you have a problem with large numbers of variables. You should be aware that sampling can induce bias in your results and to bare this in mind when interpreting data.

Use the relevant parts of OpenTURNS
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The OpenTURNS library has been developed as a way to use applied statistical methods in their most general form; it is a very comprehensive and wide ranging toolbox. Therefore, there are many aspects of the toolbox that may or may not not be relevant to you as a mechanical engineer. Furthermore, not all aspects of the toolbox are fully integrated with AsterStudy, meaning there may be relevant methods you wish to apply that will require you to work outside of salome_meca. Some of the useful tools are discussed in §4.0.1.
