##################
Walk throughs
##################

Here we have put together a set of screenshots to walk you through two aspects: porting an FE model from AsterStudy to OpenTURNS; setting up an UQ study with a symbolic model.

Porting a FE model from OpenTURNS to AsterStudy
=================================================

In practice, we assume that your FE model has been developed and is ready to be ported into OpenTURNS. The best way is that you start from a model built in AsterStudy or a command file that has been imported into AsterStudy (:numref:`importOT`). If you have imported your model, you should check that it runs in AsterStudy (:numref:`verifyOT`) because there may be syntax errors in your command file if you have previously worked with older versions of salome_meca.


.. figure:: fig5a.png
   :name: importOT
   
   Obtaining sensitivity results from OpenTURNS : Importing your command file to AsterStudy
   
.. figure:: fig5b.png
   :name: verifyOT

   Obtaining sensitivity results from OpenTURNS : Checking that your model works.

Exporting your FE model is a simple one-click process from the AsterStudy module. In the case view, simply right click and select export study to OpenTURNS (:numref:`fig6a`). If you are successful, you will find all the available input and output parameters for you to select on the right hand side of the window. There is a button for you to validate your selection that will take you into the openTURNS module and complete Step 1 of the UQ process: specification of the uncertainty problem. If your command file is not compatible, you will see errors on after clicking the export button (:numref:`fig6b`). You will have to ensure that your output parameter of interest is compatible with OpenTURNS. Remember that traditional FE studies often give you fields of data as outputs rather than single value outputs. Moreover, these outputs MUST be converted to a numpy table format for compatibility with OpenTURNS.


.. figure:: fig6a.png
   :name: fig6a
  
   Make sure that your command files are giving valid input and outputs for the OpenTURNS module, otherwise you need to modify your command files :  Succesful porting of FE study to OpenTURNS

.. figure:: fig6b.png
   :name: fig6b

   Make sure that your command files are giving valid input and outputs for the OpenTURNS module, otherwise you need to modify your command files : Unsuccesful porting of FE study to OpenTURNS

Upon a successful export of your AsterStudy model, you will find yourself in the OpenTURNS module under the Model Definition tab (:numref:`fig7`). You will need to ensure that your model is defined as a probabilistic one by right-clicking your case on the left hand panel and selecting the model definition type (:numref:`fig7`). You will notice that now you are back in the OpenTURNS module and you should have completed Step 1 of the UQ process. You should continue to setup the subsequent steps of your UQ study as per §3.2 of this document.

.. figure:: fig7.png
   :name: fig7

   Completing the UQ Step 1 under the Model Definition tab of the interactive widget in OpenTURNS.

Walk through a UQ study
==========================

Stage 1 - Model definition
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Iis the first tab of the interactive widget available to you (:numref:`fig8`). Under this tab, the user can choose to define a symbolic mode, a python model, a YACS model etc. The symbolic model is the simplest as it allows the definition of analytical models from mathematical expressions, hence, is always useful for setting up an UQ study.

.. figure:: fig8.png
   :name: fig8
  
   The Model definition tab of the interactive widget is used to define stage 1 of UQ.

:numref:`fig9a` shows the choices which are modifiable; here you are looking to setup the number of input variables and output variables of interest from your study. If your model represents a structural analysis, there may be some simple expressions which you can use to give some physical sense to your model, e.g. an analytical expression for beam deflection. In order to enable the sub-sequent stages of a UQ study, your model needs to be converted to a probabilistic one. Figure 9b shows how to do this in the model definition tab, and as you will see, you have the choice to convert one or more of all the input variables you have specified as a probabilistic input.


.. figure:: fig9a.png
   :name: fig9a

   Available choices in model definition and conversion of variables to probabilistic. (a) Model definition options.
  
.. figure:: fig9b.png
   :name: fig9b

   Available choices in model definition and conversion of variables to probabilistic. (b) Model definition tab options.



Stage 2 - Specification of the input variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This is done using the PMD tab which is only available if a set of probabilistic input variables have been defined in Stage 1. From the drop-down menus (:numref:`fig10`), you can choose from a wide range of probability density functions to specify each input marginal. It is sensible here to utilise expert guidance or consult the literature for the most appropriate pdf to model your input variable. If you have some input data, you can use some statistical tests from the OpenTURNS library to help set the parameters for your input marginal.


.. figure:: fig10.png
   :name: fig10

   Input marginal specification in the PMD tab.

Stage 3 - Propagation of uncertainties through the system
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To do this you need a sample set to be taken from your input marginals. The sample set can be generated in the DOE tab of the graphical widget (:numref:`fig11a`). Once you have created a sample, you will be able to inspect the distribution of the input data set using the tabs within the DOE tab (:numref:`fig11b`). You can access to the data representing statistical correlation between variables and inspect the dataset visually on classical graphs or with spider plots. These tabs are useful to help you understand your input data set, and particularly for you to scrutinise the data set for unwanted bias. The act of propagation of the uncertainties is linked with Stage 4.

.. figure:: fig11a.png
   :name: fig11a

   Generating a sample set for Stage 3 of UQ : Selection of sample type.

.. figure:: fig11b.png
   :name: fig11b
   
   Generating a sample set for Stage 3 of UQ : Propagating uncertainties through your system.

Stage 4 - Ranking of sensitivities
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is simply done by moving back to the PMD tab and clicking in the sensitivity sub-tab of the graphical widget. The propagation of uncertainties will happen when you click run (:numref:`fig12a`), and upon completion you will have access to the sensitivity analysis results (:numref:`fig12b`). You will have access to the First order indices, which give you an idea of the sensitivity of the particular output to variance in each input variable in isolation. You also have access to the Total order index, which gives you the sensitivity of the particular output to variance in each variable but considering interactions with all other variables.


.. figure:: fig12a.png
   :name: fig12a

   Obtaining sensitivity results from OpenTURNS. (a) Running UQ.


.. figure:: fig12b.png
   :name: fig12b
   
   Obtaining sensitivity results from OpenTURNS. (b) Accessing sensitivity indices.

Example: UQ study on a fracture mechanics assessment
======================================================

Now that you are more familiar with setting up UQ studies in OpenTURNS, it is useful to have a look at the main purpose of integrating OpenTURNS with salome_meca: running UQ studies on FE models.
For the example here, we are working with an FE model that has been setup to perform a fracture mechanics assessment. This is a typical problem for mechanical engineers that works well for an UQ study. If you are unfamiliar with a fracture mechanics assessment, essentially we are interested in assessing the stability of a structure containing defects (cracks) under loading. The FE model is used to stress a cracked body, and we calculate a fracture mechanics parameter defining the conditions at the crack tip using some in-built functions of code_aster, e.g. CALC_G in this case. This parameter, G (energy release rate), is simply compared to a critical value as a pass or fail criteria.


.. figure:: fig13.png
   :name: fig13
   
   Illustration of the UQ problem on the fracture mechanics problem.

In this particular case, we have a problem with elasto-plastic material properties and we are intending to run an UQ study which assesses the sensitivity of the output G value to natural variation in the plastic parameters. Our model has inelastic behaviour defined as σ = A + Bnp. Hence, we are assessing the sensitivity of G to natural variation in A, B, and n. As you can see in :numref:`fig14`, our problem conforms with the description in our introduction (:numref:`descmod`). As you will have noticed, this feels very much like the definition of UQ Step 1 - Specification of the uncertainty problem.

Following the steps described in §3.2, an example of a probabilisitc structural integrity assessment that can be produced is shown here; a "limit state" reliability analysis. Given the variation defined by the input marginals, it is possible to observe the distribution of outputs obtained through typical fracture assessments, e.g. energy release rate computations. As shown in :numref:`fig14`, the probability density of the fracture assessments can be compared to a deterministic limiting criterion. The confidence limits around this type of analysis can also be seen in :numref:`fig15`. These types of analyses are relevant to performing target reliability assessments, which are probabilistic working principles for structural integrity assessments [2].

.. figure:: fig14.png
   :name: fig14

   Example of a limit state analysis performed on a fracture mechanics assessment

.. figure:: fig15.png
   :name: fig15

   Example of the confidence limits on a probabilistic fracture assessment
