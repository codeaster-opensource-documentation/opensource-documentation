#############################################
What is Uncertainty Quantification?
#############################################


Definition
===========

As a basic description, UQ is an applied statistical method for quantitative assessment of sources of uncertainty within mathematical models. In the context of deterministic engineering models, this allows the analyst to gain insight into the reliability of the models as well as key parameters which contribute to the variability of the model outputs. In other words, UQ empowers the analyst to understand the influence of variance in models inputs on the variance of model outputs (:numref:`descmod`). This is beneficial for safety critical engineering assessments where regulators may question the reliability of the justification behind design choices.


.. figure:: fig1.png
   :name: descmod

   Description of a model with inputs and outputs used in uncertainty quantification.
   



UQ as a process can be characterised by four basic stages [1]:
    1. Specification of the uncertainty problem: the boundaries or scope of the problem of interest are defined, from a mathematical perspective; as illustrated by :numref:`descmod`. The analyst must first define the model to be used during the assessment, G(X). This model can take any form, such as a full scale finite element model or a reduced order model, but it must have the property that it takes a set of inputs, X, and transforms them into an output, Y . OpenTURNS allows the user to port the FE model from AsterStudy in one click, or to build a model using python, or using a symbolic model. Some tips on how to work with this are given in §3. The analyst must define the set of deterministic inputs, d, and random inputs, X = [X1,X2,...Xd], that are to be used to assess the quantity of interest. This quantity is normally a variance in or probability of an outcome.

    2. Quantification of the model input uncertainties: the analyst must quantify the uncertainties within the input variables. This involves describing each input variable with a specific probability density function (PDF), which is also known as an input marginal. This point is critical as the analyst must make an appropriate decision on how to describe the input quantities, and requires insight into the problem being modelled to make an appropriate choice. Within OpenTURNS, the analyst has a choice of many types PDFs to pick, e.g. Normal, Beta, Gaussian. OpenTURNS also features statistical tools which help the analyst making a decision here, e.g. using the Anderson-Darling or Kolmogorov-Smirnov test for normality of a distribution.

    3. Propagation of the uncertainties through the model: the uncertainties in the input variables are propagated through the system. This requires a sample set to be taken from the input PDFs. Depending on the definition of the model, G(X), this has the potential to result in an extremely large computational problem. OpenTURNS features a range of model reduction techniques in order for the analyst to be able to perform the assessments efficiently, e.g. response surface generation. Some tips on how to work efficiently to develop an UQ analysis are given in §2.2.

    4. Ranking of the importance of the uncertainties: some form of ranking of the effects of the input uncertainties on the output response must be performed. This can be conducted in a range of different ways, and OpenTURNS has many common place options available to the analyst, e.g. derivative based approaches like the ANOVA method or Monte-Carlo based estimations like the Kriging approach. The key aspect is to describe the relative importance of input variables mathematically. OpenTURNS allows the analyst to assess the influence of variables in isolation and their influence featuring any interactions with all other input variables (high order interactions).
   

