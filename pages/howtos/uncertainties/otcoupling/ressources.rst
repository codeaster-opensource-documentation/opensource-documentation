#############################################
Helpful Resources
#############################################

Some information on relevant aspects of OpenTURNS and the applied methodologies are listed here.
When using specific techniques and operators within the OpenTURNS toolbox, it is always useful to refer to the `official documentation <http://openturns.github.io/openturns/master/contents.html#hyperlinks>`__. Just note that the descriptions of the methods are mathematical and if you are not comfortable with reading the concise definitions on the OpenTURNS website, you may consider some video tutorials available on Youtube.

Uploaded by the "mathematical monk", the content is in fact produced by the academic Jeffrey W. Miller from Duke University who has a background of research in statistical sciences, applied mathematics and Bayesian methods. From over 200 videos `in the Youtube channel <https://www.youtube.com/channel/UCcAtD_VYwcYwVbTdvArsm7w#hyperlinks>`__, there are many good lectures on the fundamental concepts of statistical mathematics, and some useful introductory lectures to broader ranges of topics that will help you get to grips with the application of the mathematical concepts.

Information on the application of model reduction techniques for mechanical problems that are varying in spatial and time coordinates can be found in a paper that compares the performance of polynomial chaos and proper orthogonal decomposition and Gaussian process regression methodologies [3].
Comprehensive guidance on performing uncertainty quantification is published by the `Bureau International des Poids et Mesures (BIPM) <https://www.bipm.org/ utils/common/documents/jcgm/JCGM_100_2008_E.pdf#hyperlinks>`__ and should be consulted for details on topics that are touched upon here.
