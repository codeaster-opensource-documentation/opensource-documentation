##########################################################################################
Useful OT features for mechanical engineers
##########################################################################################

If you are a mechanical engineer with little to no prior knowledge of UQ or the OpenTURNS toolbox, at this stage you may be wondering which parts are available within salome_meca 2019, and indeed what parts you might need to use in practice.

What has been presented here is specifically about UQ. The sources of uncertainty are known to be either aleatoric or epistemic, the former being statistical uncertainty whereas the latter being related to those things which in principle we could know but do not. For example, our model, G(X), may not include all the relevant physics to model a problem with sufficient reliability. To be clear, OpenTURNS in salome_meca treats only the aleatoric uncertainties and does not support you in determining the epistemic uncertainty. UQ here should be seen as the propagation of parametric uncertainties from inputs, X, to the outputs, Y , of a deterministic computer model, Y = G(X).

Functionalities available within salome_meca
===============================================

Sensitivity indices
^^^^^^^^^^^^^^^^^^^^

They give you a measure of the influence of the variance of input parameters on the variance in output response of the structure. The first order index gives you an idea of the sensitivity in output response due to variation of individual input parameters in isolation. The total order index gives you an idea of the sensitivity in output response due to variation of individual input parameters together with all combinations of interaction with the other input parameters at the same time. These quantities are valuable in understanding the reliability of you model predictions and are useful when justifying the significance of your results. This is one of the key elements available to you within salome_meca. However, it is only available for single value outputs and it may be that you are more interested in fields of data. If this is the case, OpenTURNS can still help you but you would need to work outside of the salome_meca environment to perform your studies and you may require the use of specific model reduction techniques.


Meta-model
^^^^^^^^^^^

Itis a term used to describe model reduction. This functionality is useful if your model, G(X), is particularly large or computationally demanding. In general, if you are operating on a desktop, and your model is a FE model, then it is likely each study will take a few minutes. For detailed sensitivity studies, you are considering large numbers of simulations, hence, it is very useful to make use of meta-models in order to facilitate this. OpenTURNS in salome_meca has the functionality of extracting a meta-model on G(X) under the DOE sub-tab of the graphical widget. Using an appropriate design of experiments, you can obtain a meta-model that is validated against a test-set of data, all with one click. The model is accessible in python format and you can work with this meta-model directly and perform sensitivity studies on it directly rather than the full FE model. As with the comment on sensitivity, the integrated functionality is only available for single value output models. If you wish to create meta-models on fields of data, this can be done outside of salome_meca with OpenTURNS (see §5 for more information).

Limit reliability analysis 
^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is a functionality that is particularly useful for probabilistic safety assessment, also known as target reliability analysis. Here OpenTURNS in salome_meca allows you to obtain the probability of your model output satisfying a threshold criteria. For example, in the study in §3.3, the example was a fracture mechanics assessment. In practice, the fracture mechanics parameter evaluated is compared to a threshold criteria in order to determine if the structure is safe or unsafe. Hence, given a statistically representative sample size, you can obtain a probability of failure of your structure under the conditions with a specified confidence interval. Engineering procedures, such as ASME, provide guidance on probabilistic safety assessment, and this type of assessment is enabled in salome_meca.

Functionalities available outside salome_meca
===============================================

Input marginal tests
^^^^^^^^^^^^^^^^^^^^^^
They are there to help you determine whether or not a specific probability density function (PDF) is appropriate. If you haŝve some data, you are able to use OpenTURNS functions to determine whether or not it satisfies any PDF. There are tools like the Kolmogorov-Smirnov test of normality available to help you decide if your assumption on input marginal is appropriate. For many applications, you may not have enough data to satisfy any particular test mathematically for the confidence limit you desire. In these cases, you could be better off using a kernel density estimate (KDE) in order to fit a distribution to your data; this is how your output values are given a density function within the DOE tab of the interactive widget. However, you cannot use this functionality to model your input distribution unless you use it outside of salome_meca.
