#########
How to's
#########

This is a a collection of open source Guidelines for different studies. They are separated in differents topics
    

*****************
Thermomechanics
*****************


.. toctree::
    thermomechanics/index
    :maxdepth: 1

**********************************
Civil Engineering
**********************************

**********************************
Additive Manufacturing
**********************************

**********************************
Welding
**********************************

*****************
ROM
*****************

******************************
Coupling with other softwares
******************************
.. toctree::
    :maxdepth: 1

    
    uncertainties/otcoupling.rst
