#####################################################
salome_meca and code_aster open source documentation
#####################################################

.. note::
    This is a a collection of open source guidelines for different studies.


**Table of contents**

.. toctree::
    :maxdepth: 1

    howtos/index
    tutorials/index
    verification/index
    validation/index
    benchmarks/index

- Other documents are available about `Installation and development <https://gitlab.com/codeaster-opensource-documentation/opensource-installation-development>`_.
