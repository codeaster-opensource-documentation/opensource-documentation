# salome_meca and code_aster open source documentation

This documentation aims at sharing between the different members of the
open source community of code_aster and salome_meca. It is open-source
and available on these repositories:

- [Installation and development][1]: Guides to install salome_meca, code_aster
and a *Getting started* for development.

- [How To's, Tutorials and Examples][2]: Collection of how to's, tutorials
and examples of salome_meca usages (**this repository**).

To read this pages on GitLab, one recommends to set in your GitLab account *Preferences*,
*Behavior* / *Project overview content* to *Readme*.

## Study Guides for salome_meca

### How To's

This is a collection of open source guidelines for different studies.

### Tutorials

### Verification

### Validation

### Benchmarks

### Contributing

Anyone can contribute to this documentation as long as you follow the
rules. Since it is currently being put in place, the rules are not all
set yet. See [How to contribute][3] from [Installation and development][1].

[1]: ../../../../opensource-installation-development
[2]: ../../../../opensource-documentation
[3]: ../../../../opensource-installation-development/contribute.md
