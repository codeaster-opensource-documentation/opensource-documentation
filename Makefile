SHELL = /bin/bash

default: help

# --- build of Singularity images
# usage:
#	- set proxy environment if necessary
#	- change build directory if /tmp is too small
# 		BUILD_FLAGS="--tmpdir=/local00/tmp" make <image>.sif

# --- Sphinx doc for GitPages
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = pages
BUILDDIR      = _build

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)


.PHONY: Makefile
